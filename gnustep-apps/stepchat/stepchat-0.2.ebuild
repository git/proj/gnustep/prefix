# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="prefix"

inherit gnustep-2

S="${WORKDIR}/Etoile-${PV}/Services/User/Jabber"

DESCRIPTION="instant messenger for jabber"
HOMEPAGE="http://www.etoile-project.org/etoile/mediawiki/index.php?title=Applications"
SRC_URI="http://download.gna.org/etoile/etoile-${PV}.tar.gz"

LICENSE="BSD"
KEYWORDS="~x86 ~amd64"
SLOT="0"
