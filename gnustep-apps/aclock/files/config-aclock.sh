#!/usr/bin/env bash
echo "defaults write AClock SmoothSeconds YES"
defaults write AClock SmoothSeconds YES
echo "defaults write AClock RefreshRate 0.1"
defaults write AClock RefreshRate 0.1
